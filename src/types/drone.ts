export enum DroneState {
  IDLE = "IDLE",
  LOADING = "LOADING",
  LOADED = "LOADED",
  DELIVERING = "DELIVERING",
  DELIVERED = "DELIVERED",
  RETURNING = "RETURNING",
}

export enum DroneModel {
  Lightweight = "Lightweight",
  Middleweight = "Middleweight",
  Cruiserweight = "Cruiserweight",
  Heavyweight = "Heavyweight",
}

export interface Drone {
  id?: number;
  serial_number: String;
  model: DroneModel;
  weight: Number;
  battery: Number;
  state: DroneState;
  loads?: LoadType[]
}

export interface LoadType {
  id?: number;
  name: String;
  weight: Number;
  code: String;
  image: String;
}