import express from 'express';
import controller from '../controllers/drones';
import { loadDroneValidator, registerDroneValidator, updateDroneStateValidator } from '../validations/drones';
const router = express.Router();

router.post('/drones', registerDroneValidator, controller.registerDrone);
router.post('/drones/:id/load', loadDroneValidator, controller.loadDroneMedications);
router.put('/drones/:id/update-state', updateDroneStateValidator, controller.updateDroneState);
router.get('/drones/available', controller.getAvailableDrones);
router.get('/drones/:id/check-load', controller.checkDroneLoadedMedications);
router.get('/drones/:id/check-battery', controller.checkDroneBatteryLevel);

export = router;