import { NextFunction, Request, Response } from 'express';
import Joi from "joi";

export const registerDroneValidator = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const RegisterDroneSchema = Joi.object({
      serial_number: Joi.string().max(100).required(),
      model: Joi.string().required().valid('Lightweight', 'Middleweight', 'Cruiserweight', 'Heavyweight'),
      weight: Joi.number().max(500).required(),
      battery: Joi.number().max(100).required(),
      state: Joi.string().required().valid('IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING'),
    });
    await RegisterDroneSchema.validateAsync(req.body);
    next()
  } catch (error: any) {
    res.status(400).json({
      data: "",
      message: `Error: ${error?.message || " invalid input"}`,
    });
  }
}

export const loadDroneValidator = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const RegisterDroneSchema = Joi.object({
      name: Joi.string().required().pattern(/^[a-zA-Z0-9_-]*$/),
      weight: Joi.number().required(),
      code: Joi.string().required(),
      image: Joi.string().required(),
    });
    await RegisterDroneSchema.validateAsync(req.body);
    next()
  } catch (error: any) {
    res.status(400).json({
      data: "",
      message: `Error: ${error?.message || " invalid input"}`,
    });
  }
}

export const updateDroneStateValidator = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const RegisterDroneSchema = Joi.object({
      id: Joi.number().required(),
      state: Joi.string().required().valid('IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING'),
    });
    await RegisterDroneSchema.validateAsync({ ...req.body, ...req.params });
    next()
  } catch (error: any) {
    res.status(400).json({
      data: "",
      message: `Error: ${error?.message || " invalid input"}`,
    });
  }
}