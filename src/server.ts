import http from 'http';
import express, { Express } from 'express';
import morgan from 'morgan';
import cron from 'node-cron';
import * as fs from 'fs';
import routes from './routes/drones';
import client from './models/drones';
import { Drone } from './types/drone';

const router: Express = express();

router.use(morgan('dev'));
router.use(express.urlencoded({ extended: false }));
router.use(express.json());

router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'origin, X-Requested-With,Content-Type,Accept, Authorization');
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'GET PATCH DELETE POST');
    return res.status(200).json({});
  }
  next();
});

router.use('/', routes);

const logsHeader = `Drone Serial number | model | Battery Percentage | Date & Time \n`;
fs.appendFile(`${__dirname}/audit/audit.log`, logsHeader, (err) => {
  if (err) console.log(err);
  return logsHeader;
});

// log audits after every minute
cron.schedule('* * * * *', async () => {
  const drones: Drone[] = client.get('drones');
  var currentDate = new Date();
  var currentDateString = currentDate.getDate() + "." + (currentDate.getMonth() + 1) + "." + currentDate.getFullYear() + " " +
    currentDate.getHours() + ":" + currentDate.getMinutes();
  drones.forEach((drone: Drone) => {
    const log = `Drone | ${drone.serial_number} | ${drone.model} | ${drone.battery}% |  ${currentDateString} \n`;
    fs.appendFile(`${__dirname}/audit/audit.log`, log, (err) => {
      if (err) console.log(err);
      return log;
    });
  });
});

router.use((req, res, next) => {
  const error = new Error('not found');
  return res.status(404).json({
    message: error.message
  });
});

const httpServer = http.createServer(router);
const PORT: any = process.env.PORT ?? 6060;
httpServer.listen(PORT, () => console.log(`The server is running on port ${PORT}`));
