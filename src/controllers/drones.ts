import { Request, Response, NextFunction } from 'express';
import { Drone, DroneState, LoadType } from '../types/drone';
import { addDrone, getDroneById, checkDroneBattery, checkDroneLoads, fetchAvailableDrones, loadDroneLoads, updateState } from '../models/drones';

/**
 * registering a drone
 */
const registerDrone = async (req: Request, res: Response, next: NextFunction) => {
  const droneDetails: Drone = {
    serial_number: req.body.serial_number,
    model: req.body.model,
    weight: req.body.weight,
    battery: req.body.battery,
    state: req.body.state,
  }
  let response: Drone = await addDrone(droneDetails);
  return res.status(201).json({
    data: response,
  });
};


/**
 * Load drone with medicine
 */
const loadDroneMedications = async (req: Request, res: Response, next: NextFunction) => {
  const droneId = parseInt(req.params.id);
  const load: LoadType = {
    name: req.body.name,
    weight: req.body.weight,
    code: req.body.code,
    image: req.body.image,
  }

  const drone = await getDroneById(droneId);
  if (!drone) {
    return res.status(404).json({
      data: "",
      message: "Drone not found",
    });
  }

  if (drone.battery < 25) {
    return res.status(404).json({
      data: "",
      message: "Drone battery is below 25%",
    });
  }

  const droneMedicineWeight = !drone?.loads ? [] : drone.loads?.map((load: any) => load.weight);
  const totalDroneMedicineWeight = droneMedicineWeight.map(Number).reduce((a, b) => a + b, 0);

  if (drone.weight < (totalDroneMedicineWeight + req.body.weight)) {
    return res.status(400).json({
      data: "",
      message: "Medicine weight exceeded the drone weight limit",
    });
  }

  let response = await loadDroneLoads(droneId, load);

  return res.status(201).json({
    data: response,
  });
};


/**
 * checking loaded medication items for a given drone
 */
const checkDroneLoadedMedications = async (req: Request, res: Response, next: NextFunction) => {
  const droneId = parseInt(req.params.id);
  const drone = await getDroneById(droneId);
  if (!drone) {
    return res.status(404).json({
      data: "",
      message: "Drone not found",
    });
  }
  let response = await checkDroneLoads(droneId);
  return res.status(200).json({
    data: response,
  });
};


/**
 * checking available drones for loading
 */
const getAvailableDrones = async (req: Request, res: Response, next: NextFunction) => {
  let availableDrones: Drone[] = await fetchAvailableDrones();
  return res.status(200).json({
    data: availableDrones
  });
};

/**
 * check battery level for a given drone
 */
const checkDroneBatteryLevel = async (req: Request, res: Response, next: NextFunction) => {
  const droneId = parseInt(req.params.id);
  const drone = await getDroneById(droneId);
  if (!drone) {
    return res.status(404).json({
      data: "",
      message: "Drone not found",
    });
  }
  let response = await checkDroneBattery(droneId);
  return res.status(200).json({
    data: response,
  });
};

/**
 * Update drone state
 */
const updateDroneState = async (req: Request, res: Response, next: NextFunction) => {
  const droneId: number = parseInt(req.params.id);
  const state: DroneState = req.body.state;
  const drone = await getDroneById(droneId);

  if (!drone) {
    return res.status(404).json({
      data: "",
      message: "Drone not found",
    });
  }

  if (drone.battery < 25) {
    return res.status(404).json({
      data: "",
      message: "Drone battery is below 25%",
    });
  }

  let response = await updateState(droneId, state);
  return res.status(200).json({
    data: response,
  });
};

export default { registerDrone, getAvailableDrones, loadDroneMedications, updateDroneState, checkDroneLoadedMedications, checkDroneBatteryLevel };