const { InMemoryDatabase } = require('in-memory-database');
import { Drone, DroneState, DroneModel, LoadType } from '../types/drone';

const client = new InMemoryDatabase();

const initialData: Drone[] = [
  {
    id: 1,
    serial_number: Math.random().toString(36).substring(0, 15),
    model: DroneModel.Lightweight,
    weight: 500,
    battery: 72,
    state: DroneState.IDLE,
  }
];

client.set('drones', initialData);

const addDrone = async (drone: Drone): Promise<Drone> => {
  const numberOfDrones = client.get('drones').length
  const drones = client.get('drones');
  client.set('drones', [
    ...drones,
    {
      id: numberOfDrones + 1,
      ...drone
    }
  ]);
  return drone;
}

const getDroneById = async (droneId: number): Promise<Drone|null> => {
  const drones = client.get('drones');  
  const drone = drones.find((drone: Drone) => drone.id === droneId);

  if (!drone) {
    return null;
  }

  return drone;
}

const fetchAvailableDrones = async (): Promise<Drone[]> => {
  const drones = client.get('drones');
  return drones.filter((drone: Drone) => drone.state === DroneState.IDLE && drone.battery >= 25);
}

const loadDroneLoads = async (droneId: number, loads: LoadType): Promise<Drone | null> => {
  const drones = client.get('drones');
  const drone = drones.find((drone: Drone) => drone.id === droneId);

  if (!drone) {
    return null;
  }

  const droneLoadedLoads = drone?.loads || [];

  const loaddedDrone: Drone = {
    ...drone,
    loads: [...droneLoadedLoads, loads],
    state: DroneState.LOADING
  }

  const otherDrones = drones.filter((drone: Drone) => drone.id !== droneId);

  client.set('drones', [...otherDrones, loaddedDrone]);

  return loaddedDrone;
}

const checkDroneLoads = async (droneId: number): Promise<LoadType[]> => {
  const drones = client.get('drones');
  const drone = drones.find((drone: Drone) => drone.id === droneId);
  return drone && drone.loads ? drone.loads : [];
}

const checkDroneBattery = async (droneId: number): Promise<string> => {
  const drones = client.get('drones');
  const drone = drones.find((drone: Drone) => drone.id === droneId);
  return `${drone?.battery}%`;
}

const updateState = async (droneId: number, droneState: DroneState) => {
  const drones = client.get('drones');
  const drone = drones.find((drone: Drone) => drone.id === droneId);

  if (!drone) {
    return null;
  }

  const updatedDrone: Drone = { ...drone, state: droneState }

  const otherDrones = drones.filter((drone: Drone) => drone.id !== droneId);

  client.set('drones', [...otherDrones, updatedDrone]);

  return updatedDrone;
}

export default client;

export { addDrone, getDroneById, fetchAvailableDrones, loadDroneLoads, checkDroneLoads, checkDroneBattery, updateState };